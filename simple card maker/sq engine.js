
function formatting() {

    var data = document.querySelector("#inputs"),
        text = "",
        d,
    count = document.querySelector("#count").innerText,
    tally = (count * 1) + 1,
    tag_ = [". ", ""],
    author_ = [" ", " "],
    creds_ = [" (", ") "],
    article_ = ['"', '" '],
    date_ = [", ", " "],
    customFn = {
        'tag': function (words, name) {
            if(words.length === 0) {
                return "";
            } else  {
                return "<p><span class='" + name + "'>" + tally + tag_[0] + words + tag_[1] + "</span></br>";
            };
        },
        'author': function (words, name) {
            if(words.length === 0) {
                return "";
            } else  {
                return "<span class='" + name + "'>" + author_[0] + words + author_[1] + "</span>";
            };
        },
        'creds': function (words, name) {
            if(words.length === 0) {
                return "";
            } else  {
                return "<span class='" + name + "'>" + creds_[0] + words + creds_[1] + "</span>";
            };
        },
        'article': function (words, name) {
            if(words.length === 0) {
                return "";
            } else  {
                return "<span class='" + name + "'>" + article_[0] + words+ article_[1] + "</span>";
            };
        },
        'date': function (words, name) {
            if(words.length === 0) {
                return "";
            } else  {
                return "<span class='" + name + "'>" + date_[0] + words + date_[1] + "</span></br>";
            };
        },
        'link': function (words, name) {
            if(words.length === 0) {
                return "";
            } else  {
                return "<a target='_blank' href='" + words + "'><span class='" + name + "'>" + words + "</span></a></br>";
            };
        },
        'quotes': function (words, name) {
            if(words.length === 0) {
                return "";
            } else  {
                return "<span class='" + name + "'>" + words + "</span></p>";
            };
        }
    };

    for (d = 0; d < data.length; d++) {
        var input = data[d].id;
        if(typeof customFn[input] === 'function'){
            text += customFn[input](data[d].value, data[d].id);
        }
    }
    
    if (text.length < 50) {
        tally = count;
    }

    document.querySelector("#count").innerHTML = tally;
    if(text.length > 50) {
    document.querySelector("#card").innerHTML = document.querySelector("#card").innerHTML + "</br>" + text ;
    } else {
    document.querySelector("#card").innerHTML = document.querySelector("#card").innerHTML
    }
    if(text.length < 50) {
        alert("Please fill in all fields before trying to format!");
    }

    var clear_form = function () {
        if(document.querySelector("#savecreds").checked === true) { //find out the value of checkbox when its true/false...
            document.querySelector("#quotes").value = "";
            document.querySelector("#tag").value = "";
        } else {
            data.reset();
        }
    };

    clear_form();

    document.querySelector("#savecreds").checked = false;


}
